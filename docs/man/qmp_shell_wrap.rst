.. manpage-only doc; do not include in toctree.

:orphan:

qmp-shell-wrap
==============

SYNOPSIS
--------

qmp-shell-wrap [-h] [-H] [-v] [-p] [-l LOGFILE] ...

DESCRIPTION
-----------

.. autodocstring:: qemu.qmp.qmp_shell.main_wrap
   :noindex:
   :trim-summary:
   :trim-usage:

SEE ALSO
--------

Refer to qmp-shell(1) for information on syntax and operation of the
qmp-shell utility itself.
